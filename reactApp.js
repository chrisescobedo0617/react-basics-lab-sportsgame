function Team(props)  {
let shotPercentageDiv 
  if (props.stats.shots) {
  const shotPercentage = Math.round((props.stats.score / props.stats.shots) * 100)
  shotPercentageDiv =   
  <div>
    <strong>Shooting :</strong> {shotPercentage} %
  </div>
}

    return(
      <div className='Team'>
        <h2>{props.name}</h2>

        <div className="identity">
        <img src={props.logo} alt={props.name}/>
        </div>

        <div>  
        <strong>Shots:</strong> {props.stats.shots}
        </div>  

        <div>
          <strong>Score:</strong> {props.stats.score}
        </div>

        {shotPercentageDiv}

        <button onClick={props.shothandler}>Shoot!</button>
      </div>
    )
  }

class Game extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      resetCount: 0,
      homeTeamStats: {
        shots: 0,
        score: 0
      },
      visitingTeamStats: {
        shots: 0,
        score: 0
      }
    }

    this.shotSound = new Audio('./assets/images/sounds/Bounces.wav')
    this.scoreSound = new Audio('./assets/images/sounds/swish.wav')

  }

  shothandler = (team) => {
    const teamStatsKey = `${team}TeamStats`
    let score = this.state[teamStatsKey].score
    this.shotSound.play()

    if (Math.random() > 0.5) {
      score += 1

      setTimeout(() =>{
        this.scoreSound.play()
      },3200)
      
    }

    this.setState((state, props) => ({
      [teamStatsKey] : {
        shots: state[teamStatsKey].shots + 1,
        score
      }
    }))
  }

  resetGame = () => {
    this.setState((state, props) => ({
      resetCount: state.resetCount + 1,
      homeTeamStats: {
        shots: 0,
        score: 0
      },
      visitingTeamStats: {
        shots: 0,
        score: 0
      }
    }))
  }

  render() {
    return (
      <div className='Game'>
        <h1>Wecome to {this.props.venue}</h1>
        <div className='stats'>
          <Team 
            name={this.props.visitingTeam.name}
            logo={this.props.visitingTeam.logoSrc}
            stats={this.state.visitingTeamStats}
            shothandler={() => this.shothandler('visiting')}
          />

          <div className='versus'>
            <h1>VS</h1>
            <div>
              <strong>Resets:</strong> {this.state.resetCount}
              <button onClick={this.resetGame}>Reset Game</button>
            </div>
          </div>

          <Team 
            name={this.props.homeTeam.name}
            logo={this.props.homeTeam.logoSrc}
            stats={this.state.homeTeamStats}
            shothandler={() => this.shothandler('home')}
          />
        </div>
      </div>
    )
  }
}

// Deafault App component that all other compents are rendered through
function App(props){
  const raccoons = {
    name: 'Russiaville Raccoons',
    logoSrc: './assets/images/images.jpeg'
  }

  const squirrels = {
    name: 'Sheridan Squirrel',
    logoSrc: './assets/images/download.png'
  }

  const bunnies = {
    name: 'Burlington Bunnies',
    logoSrc: './assets/images/downloadBunny.jpeg'
  }

  const hounds = {
    name: 'Hammond Hounds',
    logoSrc: './assets/images/downloadHound.png'
  }
  return (
    <div className='App'>
      <Game 
        venue='Bankers'
        homeTeam={squirrels}
        visitingTeam={raccoons} 
      />
      <Game 
        venue='Lucus Oil'
        homeTeam={bunnies}
        visitingTeam={hounds}
      />
      
    </div>
  )
}

//Render the application
ReactDOM.render(
  <App />,
  document.getElementById('root')
);